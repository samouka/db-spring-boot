package com.sam.jdbcspring.jdbcspringdemo.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.jdbcspring.jdbcspringdemo.models.UserModel;
import com.sam.jdbcspring.jdbcspringdemo.repository.UserRepository;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<UserModel> getAllUsers() throws SQLException{
        return userRepository.getAllUsers();
    }

    public UserModel getUserById(final Long id) throws SQLException{
        return userRepository.getUserById(id);
    }
    
}
