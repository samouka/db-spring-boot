package com.sam.jdbcspring.jdbcspringdemo.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.sam.jdbcspring.jdbcspringdemo.models.UserModel;
import com.sam.jdbcspring.jdbcspringdemo.services.UserService;

import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;



@RestController
@RequestMapping("/users")
public class UserContoller {

    @Autowired
    private UserService userService;

    @GetMapping()
    public List<UserModel> getAllUsers() throws SQLException{
        return userService.getAllUsers();
    }

    @GetMapping("{id}")
    public UserModel getUserById(@PathVariable Long id) throws SQLException{
        return userService.getUserById(id);
    }
    

    
    
}
