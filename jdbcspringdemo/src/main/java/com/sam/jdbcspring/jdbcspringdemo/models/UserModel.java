package com.sam.jdbcspring.jdbcspringdemo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class UserModel {
    
    private Long id;
    private String username;
    private String password;

    public static final UserModel NULL = UserModel.from((long) -1, "", "");

    public UserModel(final Long id, final String username, final String password){
        this.id = id;
        this.username = username;
        this.password = password;
    }

     public UserModel(final String username, final String password){
        this.username = username;
        this.password = password;
    }

    public static UserModel from(final Long id, final String username, final String password){
        return new UserModel(id,username,password);
    }

     public static UserModel from(final String username, final String password){
        return new UserModel(username,password);
    }

}
