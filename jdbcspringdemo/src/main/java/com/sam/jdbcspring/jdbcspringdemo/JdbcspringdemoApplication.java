package com.sam.jdbcspring.jdbcspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JdbcspringdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JdbcspringdemoApplication.class, args);
	}

}
