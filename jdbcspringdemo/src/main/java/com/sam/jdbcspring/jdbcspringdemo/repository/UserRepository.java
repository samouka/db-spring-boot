package com.sam.jdbcspring.jdbcspringdemo.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sam.jdbcspring.jdbcspringdemo.models.UserModel;

@Repository
public class UserRepository {

    @Autowired
    private DataSource dataSource;


    private static final String GET_ALL_USERS = "SELECT * FROM USERS";
    private static final String GET_USER_BY_ID = "SELECT * FROM USERS WHERE ID=?";

    public List<UserModel> getAllUsers() throws SQLException{

        final List<UserModel> users = new ArrayList<>();

        try(Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(GET_ALL_USERS)){

                    final ResultSet rs = statement.executeQuery();
                    while(rs.next()){
                        users.add(UserModel.from(rs.getLong("id"), rs.getString("username"), rs.getString("password")));

                    }
        }
        return users;
    }

   public UserModel getUserById(final Long id) throws SQLException{
    UserModel user = UserModel.NULL;
    try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(GET_USER_BY_ID)){
                statement.setLong(1, id);
                final ResultSet rs = statement.executeQuery();
                while(rs.next()){
                    user = UserModel.from(rs.getLong("id"), rs.getString("username"), rs.getString("password"));
                }
    }
    return user;
   }
    
}
