package com.sam.springjpa.springjpademo.controllers;

import org.springframework.web.bind.annotation.RestController;

import com.sam.springjpa.springjpademo.models.UserModel;
import com.sam.springjpa.springjpademo.services.UserService;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;





@RestController
@RequestMapping("/users")
public class JpaController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserModel> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public Optional<UserModel> getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }
    
}
