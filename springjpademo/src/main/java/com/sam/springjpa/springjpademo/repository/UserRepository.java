package com.sam.springjpa.springjpademo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sam.springjpa.springjpademo.models.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long>{
    
}
